//
//  main.cpp
//  algolines
//
//  Created by Pavel Guganov on 14/08/2012.
//  Copyright (c) 2012 Pavel Guganov. All rights reserved.
//

#include <iostream>

#define WIDTH   6
#define HEIGHT  4

bool searchElement (int el, int elements[HEIGHT][WIDTH], int x, int y, int *arr, int *curEl);

int main(int argc, const char * argv[])
{

    int arr[HEIGHT][WIDTH] = {  0,0,0,1,0,1,
                                0,0,0,1,1,1,
                                1,0,0,1,1,1,
                                0,0,0,1,1,1};
    
    int arrOut[256];
    
    int curEl = -1;
    searchElement(1, arr, 3, 1, arrOut, &curEl);
    
    for (int i = 0; i < curEl; i+= 2){
        printf("x: %d  y: %d \n", arrOut[i], arrOut[i+1]);
    }
    
    return 0;
}

//el - значение элемента, которые надо искать
//elements - исходный массив
//x - тек позиция по x
//y - тек позиция по y
//arr - массив содержащий x,y по порядку искомых элементов
//curEl - количество элементов в arr массиве
bool searchElement (int el,int elements[HEIGHT][WIDTH], int x, int y, int *arr, int *curEl){
    printf("1. curx: %d  cury: %d \n", x, y);
    if (x < 0 || x > WIDTH - 1)
        return false;
    
    if (y < 0 || y > HEIGHT - 1 )
        return false;
    
    printf("2. curx: %d  cury: %d \n", x, y);
    
    if (el == elements[y][x]){
        
        printf("3. curx: %d  cury: %d \n", x, y);
        
        for (int i = 0; i < *curEl; i+= 2){
            if (arr[i] == x && arr[i+1] == y) {
                printf("3.5 curx: %d  cury: %d \n", x, y);
                return false;
            }
        }
        
        printf("4. curx: %d  cury: %d \n", x, y);
        
        arr[++(*curEl)] = x;
        arr[++(*curEl)] = y;
        
    }else {
        return false;
    }
    
    searchElement (el, elements,x-1, y, arr, curEl);
    searchElement (el, elements,x+1, y, arr, curEl);
    searchElement (el, elements,x, y-1, arr, curEl);
    searchElement (el, elements,x, y+1, arr, curEl);
    
    return true;
}

